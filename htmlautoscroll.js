var scrollDir = sessionStorage.getItem('direction') || 1
var scrollTimer;
var scrollIgnoreTime = 0;
var pauseScroll = false;
var scrollIgnoreTime;

window.htmlautoscrollEventListener = function(evt) {
    pauseScroll = true;
}

function htmlautoscrollMore(speed, extremityWait, extremityCb) {
    // console.log("y=", window.pageYOffset, "speed=", speed, "direction=", scrollDir);

    if (pauseScroll) {
        pauseScroll = false;
        htmlautoscrollStop()

        setTimeout(() => {
            sessionStorage.setItem('scroll_position', window.pageYOffset);
            htmlautoscrollStart(speed, extremityWait, extremityCb, scrollIgnoreTime)
        }, scrollIgnoreTime);

        return;
    }

    // do not use scrollBy because it rounds offset to integer **on some navigators only**.
    // Since speed might be non-integer, we want to keep the floating part too.
    window.scrollTo(0, window.pageYOffset + scrollDir * speed);
    sessionStorage.setItem('scroll_position', window.pageYOffset);
    var current = window.pageYOffset + window.innerHeight
    var body = document.body;
    var html = document.documentElement;
    var height = Math.max(body.scrollHeight, body.offsetHeight,
        html.clientHeight, html.scrollHeight, html.offsetHeight);

    // Some navigator (eg Chrome Android) does not round current to integer, so be safe with 1px margin
    var reachedBottom = scrollDir > 0 && current + 1 >= height;
    var reachedTop = scrollDir < 0 && current <= window.innerHeight + 1;

    if (reachedTop || reachedBottom) {
        if (extremityCb) {
            extremityCb(reachedBottom);
        }
        var nextDir = scrollDir * -1;
        scrollDir = 0;
        setTimeout(function () {
            scrollDir = nextDir;
            sessionStorage.setItem('direction', scrollDir)
        }, extremityWait)
    }
}

function htmlautoscrollStart(speed, extremityWaitMs = 2000, extremityCb, scrollIgnoreTimeMs = 5000) {
    htmlautoscrollStop();

    scrollIgnoreTime = scrollIgnoreTimeMs;
    document.getElementById("inner-html").addEventListener("click", window.htmlautoscrollEventListener);

    const remembered = Number.parseFloat(sessionStorage.getItem('scroll_position'));
    if (remembered) window.scrollTo(0, remembered);

    if (speed > 0) {
        scrollTimer = setInterval(
            htmlautoscrollMore,
            40, // frequency
            Math.max(speed, 1),
            extremityWaitMs,
            extremityCb
        );
    }
}

function htmlautoscrollStop() {
    document.getElementById("inner-html").removeEventListener("click", window.htmlautoscrollEventListener);

    if (scrollTimer) {
        clearInterval(scrollTimer);
    }
}