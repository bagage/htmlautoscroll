# htmlautoscroll

Automatically scroll top-to-bottom and bottom-to-top a webpage. 
[DEMO](https://bagage.gitlab.io/htmlautoscroll/) 

## install

```sh
$ yarn add htmlautoscroll
# or
$ npm install --save htmlautoscroll
```

## use

```html
<script src="htmlautoscroll.js"></script>

<script type="text/javascript">
// start scrolling
htmlautoscrollStart(
  10, // scrolling speed in pixel per sec
  2000, // time to wait when an extremity is reached in milliseconds
  (bottom) => console.log('reached', bottom ? 'bottom' : 'top') // callback when an extremity is reached
);

// stop scrolling
htmlautoscrollStop();
</script>
```

## license

[MIT](http://opensource.org/licenses/MIT)
